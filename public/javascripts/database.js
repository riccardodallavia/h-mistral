const mysql = require('mysql');

//Creating a connection to the database
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'admin',
    password: 'admin',
    database: 'mistral'
});
const user_id = 1; //let's say we know somehow the user id

/**
 * Retrieve and return the list of calls
 * @returns {Promise<json>} the list of calls
 */
function getCallList() {
    const query = 'SELECT *, IFNULL( (SELECT 1 FROM bookmarks WHERE user_id=? AND call_id=calls.id), 0) AS bookmark FROM calls ORDER BY bookmark DESC, date DESC';

    return new Promise((resolve, reject) => {
        connection.query(query, [user_id], (error, data) => {
            if (error) reject("There was a problem while trying to retrieve the call list. Please try again later.");
            resolve(data);
        });
    });
}

/**
 * Add a new call to the database with the given parameters
 * @param customer {string} the customer name
 * @param title {string} the call title
 * @param type {string} the call type
 * @returns {Promise<string>} the response message
 */
function addCall(customer, title, type) {
    const query = 'INSERT INTO calls (customer, title, type) VALUES (?, ?, ?)';

    return new Promise((resolve, reject) => {
        connection.query(query, [customer, title, type], (error) => {
            if (error) reject("There was a problem while trying to add the new call. Please try again later.");
            resolve("The new call has been added successfully.");
        });
    });
}

/**
 * Set the call's bookmark parameter to true or false
 * @param id {number} the call id
 * @param bookmark {number} whether the call is actually bookmarked or not
 * @returns {Promise<string>} the response message
 */
function setBookmark(id, bookmark) {
    let query;

    if (bookmark) {
        query = 'DELETE FROM bookmarks WHERE user_id=? AND call_id=?';
    } else {
        query = 'INSERT INTO bookmarks (user_id, call_id) VALUES (?, ?)';
    }

    return new Promise((resolve, reject) => {
        connection.query(query, [user_id, id], (error) => {
            if (error) reject("There was a problem while trying to update the call. Please try again later.");
            resolve("The call has been updated successfully.");
        })
    })
}

exports.getCallList = getCallList;
exports.addCall = addCall;
exports.setBookmark = setBookmark;