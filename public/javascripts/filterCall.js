let callType = 'ALL';
let callTitle = '';

$(document).ready(() => {
    $('#filterTitle').on('input', () => {
        //function called every time the input text changes
        filterCallTitle($('#filterTitle').val().toUpperCase());
    });
});

function filter() {
    //show all call cards by default
    $('[call]').show().each((i, val) => {
        if (!$(val).attr('call-title').toUpperCase().includes(callTitle)) {
            $(val).hide(); //hide all call cards which don't contain the text filter
        }
    });

    if (callType !== 'ALL') {
        $('[call][call-type!="' + callType + '"]').hide(); //hide all call cards with a different type
    }
}

/**
 * Filter the list of calls by their type
 * @param type {string} the call type
 */
function filterCallType(type) {
    callType = type;
    filter();
}

/**
 * Filter the list of calls by their title
 * @param title {string} the call title
 */
function filterCallTitle(title) {
    callTitle = title;
    filter();
}