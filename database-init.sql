CREATE DATABASE IF NOT EXISTS mistral;
USE mistral;

SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS calls;
DROP TABLE IF EXISTS bookmarks;

CREATE TABLE IF NOT EXISTS users (
    id INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    nickname VARCHAR(500) NOT NULL UNIQUE
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS calls (
    id INTEGER UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    customer VARCHAR(50) DEFAULT NULL,
    title VARCHAR(500) NOT NULL,
    type VARCHAR(10) NOT NULL,
    date DATETIME NOT NULL DEFAULT NOW()
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS bookmarks (
    user_id INTEGER UNSIGNED,
    call_id INTEGER UNSIGNED,
    PRIMARY KEY (user_id, call_id),
    FOREIGN KEY (call_id)
        REFERENCES calls(id)
        ON DELETE CASCADE,
    FOREIGN KEY (user_id)
        REFERENCES users(id)
        ON DELETE CASCADE
) ENGINE=InnoDB;

SET FOREIGN_KEY_CHECKS=1;

INSERT INTO users (nickname) VALUES
    ('Riccardo'),
    ('Valentina');

INSERT INTO calls (customer, title, type, date) VALUES
    (NULL, 'Social Media Manager', 'INTERNAL', '2019-09-10'),
    ('Adidas', 'Project Manager senior', 'EXTERNAL', '2019-09-01'),
    (NULL, 'Graphic Designer senior', 'INTERNAL', '2019-09-13'),
    (NULL, 'Full Stack Developer', 'INTERNAL', '2019-07-10'),
    ('Luxottica', 'Full Stack Developer', 'EXTERNAL', '2019-06-12'),
    (NULL, 'Content Design Intern', 'INTERNAL', '2019-05-25'),
    ('Adidas', 'Strategist', 'EXTERNAL', '2019-01-01');

INSERT INTO bookmarks (user_id, call_id) VALUES
    (1, 1),
    (1, 2),
    (2, 3),
    (2, 5),
    (2, 6);