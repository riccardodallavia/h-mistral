const express = require('express');
const router = express.Router();
const db = require('../public/javascripts/database');

router.get('/', (req, res, next) => {
    const msg = req.session.msg;
    req.session.msg = undefined;

    db.getCallList().then(
        data => res.render('index', {callList: data, error: null, msg: msg}),
        error => res.render('index', {callList: null, error: error, msg: msg})
    );
});

router.post('/', (req, res, next) => {
    let promise;

    if (req.body.action === "addCall") {
        const call_title = req.body.call_title;
        const call_type = req.body.call_type;
        const call_customer = req.body.call_customer || null;

        if (!call_title) {
            promise = Promise.reject("Call title cannot be empty.");
        } else if (call_type === 'EXTERNAL' && !call_customer) {
            promise = Promise.reject("Call customer cannot be empty.");
        } else {
            promise = db.addCall(call_customer, call_title, call_type);
        }
    } else if (req.body.action === "setBookmark") {
        const call_id = parseInt(req.body.call_id);
        const call_bookmark = parseInt(req.body.call_bookmark);

        promise = db.setBookmark(call_id, call_bookmark);
    }

    promise.then(
        success => req.session.msg = [success, false],
        error => req.session.msg = [error, true]
    ).finally(
        () => res.redirect('/')
    );
});

module.exports = router;
